export default {
  setIssues(state, issues) {
    state.issues = issues;
  },
  setAssignedMRs(state, mrs) {
    state.mrsAssignedToMe = mrs;
  },
  toggleLoading(state) {
    state.isLoading = !state.isLoading;
  },
};
