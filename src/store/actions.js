import axios from 'axios';
const glResource = axios.create({
  baseURL: 'https://gitlab.com/api/v4/',
  headers: {
    'PRIVATE-TOKEN': localStorage.getItem('GL_TOKEN'),
  },
});

export default {
  fetchIssues({ commit }) {
    glResource.get('issues?scope=assigned-to-me&state=opened').then(res => {
      commit('setIssues', res.data);
      commit('toggleLoading');
    });
  },
  fetchAssignedMRs({ commit }) {
    glResource
      .get('merge_requests?scope=assigned-to-me&state=opened')
      .then(res => {
        commit('setAssignedMRs', res.data);
      });
  },
};
